#include <Adafruit_NeoPixel.h>

#define NUM_PIXELS 8
#define WAIT 100
Adafruit_NeoPixel pixels(NUM_PIXELS, D2, NEO_GRB | NEO_KHZ800);

void setup() {
  pixels.begin();
}

void setColor(uint32_t color) {
  for (int i = 0; i < NUM_PIXELS; i++) {
    pixels.setPixelColor(i, color);
    pixels.show();
    delay(WAIT);
  }
}

void loop() {
  setColor(pixels.Color(100, 0, 0));
  delay(WAIT);
  setColor(pixels.Color(0, 100, 0));
  delay(WAIT);
  setColor(pixels.Color(0, 0, 100));
  delay(WAIT);
  setColor(pixels.Color(100, 0, 100));
  delay(WAIT);
  setColor(pixels.Color(0, 100, 100));
  delay(WAIT);
  setColor(pixels.Color(100, 100, 0));
  delay(WAIT);
  setColor(pixels.Color(100, 100, 100));
  delay(WAIT);
}